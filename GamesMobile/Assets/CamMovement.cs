﻿using UnityEngine;
using System.Collections;

public class CamMovement : MonoBehaviour {

    public float moveSensitivityX = 1.0f;
    public float moveSensitivityY = 1.0f;
    public bool updateZoomSensitivity = true;
    public float orthoZoomSpeed = 0.05f;
    public float minZoom = 1.0f;
    public float maxZoom = 20.0f;
    public bool invertMoveX = false;
    public bool invertMoveY = false;
    public float mapWidth = 60.0f;
    public float mapHeight = 40.0f;
    private Camera cam;
    private float scrollVelocity = 0.0f;
    private float timeTouchPhaseEnded;
    private Vector2 scrollDirection = Vector2.zero;
    public float rSpeed = 10.0f;
    private float pitch = 0.0f;
    private float yaw = 0.0f;

    void Start()
    {
        cam = Camera.main;
        maxZoom = 0.5f * (mapWidth / cam.aspect);

        if (mapWidth > mapHeight)
        {
            maxZoom = 0.5f * mapHeight;
        }
        if (cam.orthographicSize > maxZoom)
        {
            cam.orthographicSize = maxZoom;
        }
    }

    void Update()
    {
        if (updateZoomSensitivity)
        {
            moveSensitivityX = cam.orthographicSize / 5.0f;
            moveSensitivityY = cam.orthographicSize / 5.0f;
        }

        Touch[] touches = Input.touches;
        if (touches.Length > 0)
        {
            if (touches.Length == 1)
            {
                if (touches[0].phase == TouchPhase.Began)
                {
                    scrollVelocity = 0.0f;
                }
                else if (touches[0].phase == TouchPhase.Moved)
                {
                    Vector2 delta = touches[0].deltaPosition;
                    float positionX = delta.x * moveSensitivityX * Time.deltaTime;
                    positionX = invertMoveX ? positionX : positionX * -1;
                    float positionY = delta.y * moveSensitivityY * Time.deltaTime;
                    positionY = invertMoveY ? positionY : positionY * -1;
                    cam.transform.position += new Vector3(positionX, positionY, 0);
   
                    scrollDirection = touches[0].deltaPosition.normalized;
                    scrollVelocity = touches[0].deltaPosition.magnitude / touches[0].deltaTime;

                    if (scrollVelocity <= 100)
                    {
                        scrollVelocity = 0;
                    }
                }
                else if (touches[0].phase == TouchPhase.Ended)
                {
                    timeTouchPhaseEnded = Time.time;
                }
            }

            if (touches.Length == 2)
            {
                Vector2 cameraViewsize = new Vector2(cam.pixelWidth, cam.pixelHeight);

                Touch touchOne = touches[0];
                Touch touchTwo = touches[1];

                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                Vector2 touchTwoPrevPos = touchTwo.position - touchTwo.deltaPosition;

                float prevTouchDeltaMag = (touchOnePrevPos - touchTwoPrevPos).magnitude;
                float touchDeltaMag = (touchOne.position - touchTwo.position).magnitude;

                float deltaMagDiff = prevTouchDeltaMag - touchDeltaMag;

                cam.transform.position += cam.transform.TransformDirection((touchOnePrevPos + touchTwoPrevPos - cameraViewsize) * cam.orthographicSize / cameraViewsize.y);

                cam.orthographicSize += deltaMagDiff * orthoZoomSpeed;
                cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, minZoom, maxZoom) - 0.001f;

                cam.transform.position -= cam.transform.TransformDirection((touchOne.position + touchTwo.position - cameraViewsize) * cam.orthographicSize / cameraViewsize.y);

            }
            if(touches.Length == 3)
            {
                pitch -= Input.GetTouch(0).deltaPosition.y * rSpeed * Time.deltaTime;
                yaw += Input.GetTouch(0).deltaPosition.x * rSpeed * Time.deltaTime;
                pitch = Mathf.Clamp(pitch, -30, 30);
                yaw = Mathf.Clamp(yaw, -30, 30);
                this.transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
            }
            if (touches.Length == 4)
            {
                Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
                pos = Input.mousePosition - pos;
                float ang = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - 0.0f;
                transform.rotation = Quaternion.AngleAxis(ang, Vector3.forward);
            }
        }
    }
}
    
