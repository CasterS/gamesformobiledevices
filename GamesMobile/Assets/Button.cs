﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
    [SerializeField]
    float _hLimit = 2.5f, _vLimit = 2.5f, dragSpeed = 0.1f;

    Transform cachedTransform;
    Vector3 startPos;
    Vector2 deltaPos;

    public Color def;
    public Color sel;
    private Material mat;
    bool selected = false;

    private float baseAngle = 0.0f;

    void Start()
    {
        cachedTransform = transform;
        startPos = cachedTransform.position;
        mat = GetComponent<Renderer>().material;
       
    }
    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (selected == true)
            {

                deltaPos = Input.GetTouch(0).deltaPosition;
                MoveObject(deltaPos);
                Rotate();
                ScaleUp();
                ScaleDown();
                GameObject.Find("MainCamera").GetComponent<CamMovement>().enabled = false;
            }
            else
            {
                GameObject.Find("MainCamera").GetComponent<CamMovement>().enabled = true;

            }

        }
    }
    void isSelected()
    {

    }
    void OnTouchDown()
    {

            if (selected == false)
            {
                selected = true;
                mat.color = sel;
            }
            else
            {
                mat.color = def;
                selected = false;
            }
 
    }
    void OnTouchUp()
    {
        //mat.color = def;
    }
    void OnTouchStay()
    {
        //mat.color = sel;
    }
    void OnTouchExit()
    {
        //mat.color = def;
    }
    void MoveObject(Vector2 deltaPos)
    {
        cachedTransform.position = new Vector3(Mathf.Clamp((deltaPos.x * dragSpeed) + cachedTransform.position.x,
                                                startPos.x - _hLimit, startPos.x + _hLimit),
                                                Mathf.Clamp((deltaPos.y * dragSpeed) + cachedTransform.position.y,
                                                startPos.y - _vLimit, startPos.y + _vLimit), cachedTransform.position.z);
    }
    void Rotate()
    {
        if (Input.touchCount == 2)
        { 
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos = Input.mousePosition - pos;
            float ang = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg - baseAngle;
            transform.rotation = Quaternion.AngleAxis(ang, Vector3.forward);
        }
    }
    void ScaleUp()
    {
        if (Input.touchCount == 3)
        {
            transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        }  
    }
    void ScaleDown()
    {
        if (Input.touchCount == 4)
        {
            transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
}