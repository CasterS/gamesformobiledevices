Ignore Scene1

open NewScene
2 sets of controls.

when no object selected 
1 finger moves camera
2 fingers zoom in and out 
3 fingers changes camera angle
4 fingers rotates camera

on start all cubes are white
on select with 1 finger touch bright colour
on deselect with 1 finger dark shade of same colour
1 finger off the cube moves object with limited vertical and horizontal settings
2 fingers rotates cube or multiple selected cubes
3 fingers scales up cube/cubes
4 fingers scales down cube/cubes